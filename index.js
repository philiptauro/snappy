var webshot = require('webshot');
var fs = require('fs');
var inquirer = require('inquirer');

var questions = [
    {
        type: 'input',
        message: 'Specify file location : ',
        name: 'linksFile'
    },
    {
        type: 'checkbox',
        message: 'Select screen size(s) : ',
        name: 'sizes',
        choices: [
            new inquirer.Separator(' = Generic = '),
            {
                name: 'Large'
            },
            {
                name: 'Medium'
            },
            {
                name: 'Small'
            },
            new inquirer.Separator(' = Devices = '),
            {
                name: 'iPhone 5'
            },
            {
                name: 'iPhone 6'
            },
            {
                name: 'iPhone 6 Plus'
            },
            {
                name: 'iPad'
            },
            {
                name: 'iPad Pro'
            }
        ]
    }
    ,
    {
        type: 'input',
        message: 'Specify input path : ',
        name: 'path'
    }
]
var deviceToDimension = {
    'Large': 'large',
    'Medium': 'medium',
    'Small': 'small',
    'iPhone 5': 'iphone5',
    'iPhone 6': 'iphone6',
    'iPhone 6 Plus': 'iphone6plus',
    'iPad': 'ipad',
    'iPad Pro': 'ipadpro'
}
var dimensions = {
    large: {
        screenSize: {
            width: 1600
        }
        , shotSize: {
            width: 1600,
            height: 'all'
        }
    },
    medium: {
        screenSize: {
            width: 1068
        }
        , shotSize: {
            width: 1068,
            height: 'all'
        }
    }
    , small: {
        screenSize: {
            width: 735
        }
        , shotSize: {
            width: 735,
            height: 'all'
        }
    }
    , iphone5: {
        screenSize: {
            width: 320,
            height: 568
        }
        , shotSize: {
            width: 320,
            height: 'all'
        }
    }
    , iphone6: {
        screenSize: {
            width: 375,
            height: 667
        }
        , shotSize: {
            width: 375,
            height: 'all'
        }
    }
    , iphone6plus: {
        screenSize: {
            width: 414,
            height: 736
        }
        , shotSize: {
            width: 414,
            height: 'all'
        }
    }
    , ipad: {
        screenSize: {
            width: 768,
            height: 1024
        }
        , shotSize: {
            width: 768,
            height: 'all'
        }
    }
    , ipadpro: {
        screenSize: {
            width: 1024,
            height: 1366
        }
        , shotSize: {
            width: 1024,
            height: 'all'
        }
    }
}
inquirer.prompt(questions).then(function (answers) {
    var linksFile = answers.linksFile.trim();
    var sizesSelected = answers.sizes;
    var sizes = [];
    var path = answers.path.toString().trim();
    var textByLine = ""
    fs.readFile(linksFile, function (err, text) {
        text = text.toString()
        textByLine = text.split("\n")
        var generateWS = [];
        var x = 0;
        textByLine.forEach(function (item) {
            x++;
            sizesSelected.forEach(function (size) {
                sizes.push(dimensions[deviceToDimension[size]]);
                screenFileFullName = path + "/link-" + x + deviceToDimension[size] + '.jpg';
                generateWS.push(webshot(item, screenFileFullName, dimensions[deviceToDimension[size]], function (err) {

                }));
            });
        });
        Promise.all(generateWS).then(function () {
            console.log("Operation completed successfully.");
        }, function() {
            console.log("An error occurred.");
        })
    });
}).catch(function (e) {
    console.log(e)
});